import nltk
import pandas as pd
import string
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
from nltk.stem import WordNetLemmatizer
from nltk.util import ngrams
from collections import Counter

lemmatizer = WordNetLemmatizer()

#read tsv file
smsTexts = pd.read_csv("SMSSpamCollection.tsv", sep='\t', header=None)
smsTexts.columns = ['lable', 'body_text']
smsTexts.head()

#change messages to lowercase
smsTexts['lower_cased'] = smsTexts['body_text'].apply(lambda x: " ".join(x.lower() for x in x.split()))

#Remove punctuation
smsTexts['punctuation_removed'] = smsTexts['lower_cased'].apply(lambda sentence: ''.join(char for char in sentence if char not in string.punctuation))

#word tokenization
smsTexts['word_tokens'] = (smsTexts['punctuation_removed']).apply(word_tokenize)

#Remove stopwords
eng_stops = stopwords.words('english')
smsTexts['stopwords_removed'] = smsTexts['word_tokens'].apply(lambda words: list(w for w in words if w not in eng_stops))

#remove non-alphabet
smsTexts['numbers_removed'] = smsTexts['stopwords_removed'].apply(lambda words: list(w for w in words if w.isalpha()))

#Lemmatization
smsTexts['lemmatized'] = smsTexts['numbers_removed'].apply(lambda words: list(lemmatizer.lemmatize(w) for w in words))

#create bigrams 
bigrams_list = []
for list in smsTexts.lemmatized:
  bigrams = []
  for i in range(0, len(list) - 1):
      bigrams.append((list[i], list[i+1]) )
  bigrams_list.append(bigrams)
	
smsTexts['bigrams'] = bigrams_list

spam_bigram_corpus = []
ham_bigram_corpus = []
for row in smsTexts.itertuples(index=True, name='Pandas'):
  if getattr(row, "lable") == 'ham':
      for bigram in getattr(row, "bigrams"):
        ham_bigram_corpus.append(bigram)
  else: 
      for bigram in getattr(row, "bigrams"):
	      spam_bigram_corpus.append(bigram)

spam_bigram_corpus = Counter(spam_bigram_corpus)
ham_bigram_corpus = Counter(ham_bigram_corpus)

spam_unigram_corpus = []
ham_unigram_corpus = []
for row in smsTexts.itertuples(index=True, name='Pandas'):
  if getattr(row, "lable") == 'ham':
	  for word in getattr(row, "lemmatized"):
	    ham_unigram_corpus.append(word)
  else:
      for word in getattr(row, "lemmatized"):
	      spam_unigram_corpus.append(word)

spam_unigram_corpus = Counter(spam_unigram_corpus)
ham_unigram_corpus = Counter(ham_unigram_corpus)		  
		
def msgPreprocess(text):
  lowercased = text.lower()
  rmv_punc = ''.join(char for char in lowercased if char not in string.punctuation)
  word_tokens = word_tokenize(rmv_punc)
  rmv_stopwords = [word for word in word_tokens if word not in eng_stops]
  lemmatized = [lemmatizer.lemmatize(word) for word in rmv_stopwords]
  bigrams = [(lemmatized[i], lemmatized[i+1]) for i in range(0, len(lemmatized) - 1)] 
  return bigrams
 
message1 = "Sorry, ..use your brain dear"
message2 = "SIX chances to win CASH."
def hamBigramProbability(msg):
  bigrams = msgPreprocess(msg)
  prob = 1
  
  for bigram in bigrams:
      try:
        N = ham_unigram_corpus[bigram[0]]
      except KeyError:
	      N = 0
	  
      try:
        c = ham_bigram_corpus[bigram[0], bigram[1]]
      except KeyError:
          c = 0
	  
      V = len(ham_unigram_corpus)
      p = (c + 1) / (N + V)
      prob = prob * p
  return prob

def spamBigramProbability(msg):
  bigrams = msgPreprocess(msg)
  prob = 1
  
  for bigram in bigrams:
      try:
        N = spam_unigram_corpus[bigram[0]]
      except KeyError:
	      N = 0
	  
      try:
        c = spam_bigram_corpus[bigram[0], bigram[1]]
      except KeyError:
          c = 0
	  
      V = len(spam_unigram_corpus)
      p = (c + 1) / (N + V)
      prob = prob * p
  return prob  

print("bigram probability of message1 in ham corpus: ", hamBigramProbability(message1)) 
print("\nbigram probability of message1 in spam corpus: ", spamBigramProbability(message1)) 

if hamBigramProbability(message1) > spamBigramProbability(message1):
  print("message1 is a ham")
else:
  print("message1 is a spam")
  
print("\n\nbigram probability of message2 in ham corpus: ", hamBigramProbability(message2)) 
print("\nbigram probability of message2 in spam corpus: ", spamBigramProbability(message2)) 
  
if hamBigramProbability(message2) > spamBigramProbability(message2):
  print("message2 is a ham")
else:
  print("message2 is a spam")

 



